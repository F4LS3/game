package de.f4ls3.game.graphics;

import de.f4ls3.game.utils.Logger;
import de.f4ls3.game.utils.Vector2f;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Sprite {

    private BufferedImage[][] spriteArray;
    private BufferedImage SPRITESHEET;
    private final int TILE_SIZE = 32;

    public int w, h;
    private int wSprite, hSprite;
    private String file;

    public Sprite(String file) {
        this.w = TILE_SIZE;
        this.h = TILE_SIZE;
        this.file = file;

        Logger.log("Loading \"" + this.file.toUpperCase() + "\" Sprite");
        this.SPRITESHEET = loadSprite(this.file);

        this.wSprite = this.SPRITESHEET.getWidth() / this.w;
        this.hSprite = this.SPRITESHEET.getHeight() / this.h;
        loadSpriteArray();
    }

    public Sprite(String file, int w, int h) {
        this.w = w;
        this.h = h;
        this.file = file;

        Logger.log("Loading \"" + this.file.toUpperCase() + "\" Sprite");
        this.SPRITESHEET = loadSprite(this.file);

        this.wSprite = this.SPRITESHEET.getWidth() / this.w;
        this.hSprite = this.SPRITESHEET.getHeight() / this.h;
        loadSpriteArray();
    }

    public void setSize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    public void setWidth(int i) {
        this.w = i;
        this.wSprite = this.SPRITESHEET.getWidth() / this.w;
    }

    public void setHeight(int i) {
        this.h = i;
        this.hSprite = this.SPRITESHEET.getHeight() / this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getHeight() {
        return this.h;
    }

    public BufferedImage loadSprite(String file) {
        BufferedImage sprite = null;
        try {
            sprite = ImageIO.read(getClass().getClassLoader().getResourceAsStream(file));

        } catch (Exception e) {
            Logger.err("Couldn't load file: " + file + ". Cause: " + e.getCause().getStackTrace());
        }

        return sprite;
    }

    public void loadSpriteArray() {
        this.spriteArray = new BufferedImage[this.hSprite][this.wSprite];

        for (int y = 0; y < this.hSprite; y++) {
            for (int x = 0; x < this.wSprite; x++) {
                this.spriteArray[y][x] = getSprite(x, y);
            }
        }
    }

    public BufferedImage getSpriteSheet() {
        return this.SPRITESHEET;
    }

    public BufferedImage getSprite() {
        return this.SPRITESHEET.getSubimage(0 * this.w, 0 * this.h, this.w, this.h);
    }

    public BufferedImage getSprite(int x, int y) {
        return this.SPRITESHEET.getSubimage(x * this.w, y * this.h, this.w, this.h);
    }

    public BufferedImage getSprite(int x, int y, int w, int h) {
        return this.SPRITESHEET.getSubimage(x * w, y * h, w, h);
    }

    public BufferedImage[] getSpriteArray(int i) {
        return this.spriteArray[i];
    }

    public BufferedImage[][] getMoreDimensionalSpriteArray(int i) {
        return this.spriteArray;
    }

    public static void drawArray(Graphics2D graphics, ArrayList<BufferedImage> images, Vector2f vec, int width, int height, int xOffset, int yOffset) {
        float x = vec.getX(), y = vec.getY();

        for (int i = 0; i < images.size(); i++) {
            if (images.get(i) != null) {
                graphics.drawImage(images.get(i), (int) x, (int) y, width, height, null);
            }

            x += xOffset;
            y += yOffset;
        }
    }

    public static void drawArray(Graphics2D graphics, Font f, String word, Vector2f vec, int width, int height, int xOffset, int yOffset) {
        float x = vec.getX(), y = vec.getY();

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) != 32) {
                graphics.drawImage(f.getFont(word.charAt(i)), (int) x, (int) y, width, height, null);
            }

            x += xOffset;
            y += yOffset;
        }
    }

}
