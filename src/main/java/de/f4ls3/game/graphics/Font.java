package de.f4ls3.game.graphics;

import de.f4ls3.game.utils.Logger;
import de.f4ls3.game.utils.Vector2f;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Font {

    private BufferedImage[][] fontArray;
    private BufferedImage FONTSHEET;
    private final int TILE_SIZE = 32;

    public int w, h;
    private int wLetter, hLetter;

    public Font(String file) {
        this.w = TILE_SIZE;
        this.h = TILE_SIZE;

        Logger.log("Loading \"" + file.toUpperCase() + "\" Sprite");
        this.FONTSHEET = loadSprite(file);

        this.wLetter = FONTSHEET.getWidth() / w;
        this.hLetter = FONTSHEET.getHeight() / h;
        loadFontArray();
    }

    public Font(String file, int w, int h) {
        this.w = w;
        this.h = h;

        Logger.log("Loading \"" + file.toUpperCase() + "\" Sprite");
        this.FONTSHEET = loadSprite(file);

        this.wLetter = FONTSHEET.getWidth() / w;
        this.hLetter = FONTSHEET.getHeight() / h;
        loadFontArray();
    }

    public void setSize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    public void setWidth(int i) {
        this.w = i;
        this.wLetter = this.FONTSHEET.getWidth() / this.w;
    }

    public void setHeight(int i) {
        this.h = i;
        this.hLetter = this.FONTSHEET.getHeight() / this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getHeight() {
        return this.h;
    }

    public BufferedImage loadSprite(String file) {
        BufferedImage sprite = null;
        try {
            sprite = ImageIO.read(getClass().getClassLoader().getResourceAsStream(file));

        } catch (Exception e) {
            e.printStackTrace();
            Logger.err("Couldn't load file: " + file + ". Cause: " + e.getCause().getStackTrace());
        }

        return sprite;
    }

    public void loadFontArray() {
        this.fontArray = new BufferedImage[this.wLetter][this.hLetter];

        for (int x = 0; x < this.wLetter; x++) {
            for (int y = 0; y < this.hLetter; y++) {
                this.fontArray[x][y] = getLetter(x, y);
            }
        }
    }

    public BufferedImage getFontSheet() {
        return this.FONTSHEET;
    }

    public BufferedImage getLetter(int x, int y) {
        return this.FONTSHEET.getSubimage(x * this.w, y * this.h, this.w, this.h);
    }

    public BufferedImage getFont(char letter) {
        int value = letter;

        int x = value % this.wLetter;
        int y = value / this.wLetter;
        return getLetter(x, y);
    }
}
