package de.f4ls3.game.graphics;

import java.awt.image.BufferedImage;

public class Animation {

    private BufferedImage[] frames;
    private int currentFrame, totalFrames, count, delay, timesPlayed;

    public Animation(BufferedImage[] frames) {
        this.frames = frames;
        this.timesPlayed = 0;
        //setFrames(frames);
    }

    public Animation() {
        this.timesPlayed = 0;
    }

    public void setFrames(BufferedImage[] frames) {
        this.frames = frames;
        this.currentFrame = 0;
        this.count = 0;
        this.timesPlayed = 0;
        this.delay = 0;
        this.totalFrames = frames.length;
    }

    public void setDelay(int i) {
        this.delay = i;
    }

    public void setFrame(int i) {
        this.currentFrame = i;
    }

    public void setTotalFrames(int i) {
        this.totalFrames = i;
    }

    public void update() {
        if(this.delay == -1) return;
        this.count++;

        if(this.count == this.delay) {
            this.currentFrame++;
            this.count = 0;
        }

        if(this.currentFrame == this.totalFrames) {
            this.currentFrame = 0;
            this.timesPlayed++;
        }
    }

    public int getDelay() {
        return this.delay;
    }

    public int getFrame() {
        return this.currentFrame;
    }

    public int getCount() {
        return this.count;
    }

    public BufferedImage getImage() {
        return this.frames[this.currentFrame];
    }

    public boolean hasPlayedOnce() {
        return this.timesPlayed > 0;
    }

    public boolean hasPlayed(int i) {
        return this.timesPlayed == i;
    }
}
