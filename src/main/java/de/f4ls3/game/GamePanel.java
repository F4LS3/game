package de.f4ls3.game;

import de.f4ls3.game.states.GameStateManager;
import de.f4ls3.game.utils.KeyHandler;
import de.f4ls3.game.utils.MouseHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class GamePanel extends JPanel implements Runnable {

    public static int oldFrameCount;

    private static int width, height, x = 0, y = 0;
    private double lastUpdateTime = System.nanoTime(), lastRenderTime;
    private boolean running;

    private Thread thread;
    private BufferedImage image;
    private Graphics2D graphics;

    private MouseHandler mouse;
    private KeyHandler key;

    private GameStateManager gameStateManager;

    private final double REFRESH_RATE = 60.0, UPDATE_DELAY = 1000000000 / this.REFRESH_RATE, TARGET_FPS = 60, TOTAL_TIME_BEFORE = 1000000000 / this.TARGET_FPS;
    private final int MUST_RENDER_BEFORE = 5;

    public GamePanel(int width, int height) {
        this.width = width;
        this.height = height;

        setPreferredSize(new Dimension(this.width, this.height));
        setFocusable(true);
        requestFocus();
    }

    @Override
    public void addNotify() {
        super.addNotify();

        if(this.thread == null) {
            this.thread = new Thread(this, "GamePanelThread");
            this.thread.start();

        } else {
            this.thread.start();
        }
    }

    public void init() {
        this.running = true;

        this.image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_ARGB);
        this.graphics = (Graphics2D) this.image.getGraphics();

        this.mouse = new MouseHandler(this);
        this.key = new KeyHandler(this);

        this.gameStateManager = new GameStateManager();
    }

    @Override
    public void run() {
        init();

        int frameCount = 0;
        oldFrameCount = 0;
        int lastSecondTime = (int) (this.lastUpdateTime / 1000000000);

        while (this.running) {
            double now = System.nanoTime();
            int updateCount = 0;
            while(((now - this.lastUpdateTime) > this.UPDATE_DELAY) && (updateCount < MUST_RENDER_BEFORE)){
                update();
                input(this.mouse, this.key);
                this.lastUpdateTime += this.UPDATE_DELAY;
                updateCount++;
            }

            if(now - this.lastUpdateTime > this.UPDATE_DELAY) {
                this.lastUpdateTime = now - this.UPDATE_DELAY;
            }

            input(this.mouse, this.key);
            render();
            draw();
            this.lastRenderTime = now;
            frameCount++;

            int thisSecond = (int) (this.lastUpdateTime / 1000000000);
            if(thisSecond > lastSecondTime) {
                if(frameCount != oldFrameCount) {
                    System.out.println("NEW SECOND " + thisSecond + " NEW FRAME " + frameCount);
                    oldFrameCount = frameCount;
                }
                frameCount = 0;
                lastSecondTime = thisSecond;
            }

            while(now - this.lastRenderTime < this.TOTAL_TIME_BEFORE && now - this.lastUpdateTime < this.UPDATE_DELAY) {
                Thread.yield();

                try {
                    Thread.sleep(1);

                } catch (Exception e) {
                    System.err.println("ERROR: Thread yield!");
                }
                now = System.nanoTime();
            }
        }
    }

    public void update() {
        this.width = this.getWidth();
        this.height = this.getHeight();
        this.gameStateManager.update();
    }

    public void input(MouseHandler mouse, KeyHandler key) {
        this.gameStateManager.input(mouse, key);
    }

    public void render() {
        if(this.graphics == null) return;

        this.graphics.setColor(new Color(66, 134, 244));
        this.graphics.fillRect(0, 0, this.width, this.height);

        this.gameStateManager.render(this.graphics);
    }

    public void draw() {
        Graphics graphics = this.getGraphics();
        graphics.drawImage(this.image, this.x, this.y, this.width, this.height, null);
        graphics.dispose();
    }

    public static int getWindowWidth() {
        return width;
    }

    public static int getWindowHeight() {
        return height;
    }
}
