package de.f4ls3.game.entity;

import de.f4ls3.game.collision.AxisAlignedBoundingBox;
import de.f4ls3.game.graphics.Animation;
import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.utils.KeyHandler;
import de.f4ls3.game.utils.MouseHandler;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Entity {

    private final int UP = 3, DOWN = 2, RIGHT = 0, LEFT = 1;

    protected Sprite sprite;
    protected Vector2f vec;
    protected Animation animation;
    protected int size;

    protected int currentAnimation, attackSpeed, attackDuration;
    protected float directionX, directionY, maxSpeed = 4f, acceleration = 3f, deceleration = 0.4f;
    protected boolean up, down, right, left, attack;
    protected AxisAlignedBoundingBox hitBounds, bounds;

    public Entity(Sprite sprite, Vector2f vec, int size) {
        this.sprite = sprite;
        this.vec = vec;
        this.size = size;

        this.bounds = new AxisAlignedBoundingBox(vec, size, size);
        this.hitBounds = new AxisAlignedBoundingBox(new Vector2f(vec.getX() + (size / 2), vec.getY()), size, size);

        this.animation = new Animation();
        setAnimation(RIGHT, sprite.getSpriteArray(RIGHT), 10);
    }

    public void setAnimation(int i, BufferedImage[] frames, int delay) {
        this.currentAnimation = i;
        this.animation.setFrames(frames);
        this.animation.setDelay(delay);
    }

    public void animate() {
        if (this.up) {
            if(this.currentAnimation != this.UP || this.animation.getDelay() == -1) {
                setAnimation(this.UP, this.sprite.getSpriteArray(this.UP), 5);
            }

        } else if (this.down) {
            if(this.currentAnimation != this.DOWN || this.animation.getDelay() == -1) {
                setAnimation(this.DOWN, this.sprite.getSpriteArray(this.DOWN), 5);
            }

        } else if (this.right) {
            if(this.currentAnimation != this.RIGHT || this.animation.getDelay() == -1) {
                setAnimation(this.RIGHT, this.sprite.getSpriteArray(this.RIGHT), 5);
            }

        } else if (this.left) {
            if(this.currentAnimation != this.LEFT || this.animation.getDelay() == -1) {
                setAnimation(this.LEFT, this.sprite.getSpriteArray(this.LEFT), 5);
            }

        } else {
            setAnimation(this.currentAnimation, this.sprite.getSpriteArray(this.currentAnimation), -1);
        }
    }

    private void setHitBoxDirection() {
        if (this.up) {
            this.hitBounds.setYOffset(-this.size / 2);
            this.hitBounds.setXOffset(-this.size / 2);

        } else if (this.down) {
            this.hitBounds.setYOffset(this.size / 2);
            this.hitBounds.setXOffset(-this.size / 2);

        } else if (this.right) {
            this.hitBounds.setXOffset(-this.size);
            this.hitBounds.setYOffset(0);

        } else if (this.left) {
            this.hitBounds.setXOffset(0);
            this.hitBounds.setYOffset(0);

        }
    }

    public void update() {
        animate();
        setHitBoxDirection();
        this.animation.update();
    }

    public abstract void render(Graphics2D graphics);

    public void input(KeyHandler key, MouseHandler mouse) { }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setAcceleration(float acceleration) {
        this.acceleration = acceleration;
    }

    public void setDeceleration(float deceleration) {
        this.deceleration = deceleration;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public AxisAlignedBoundingBox getBounds() {
        return bounds;
    }

    public int getSize() {
        return this.size;
    }

    public Animation getAnimation() {
        return this.animation;
    }
}
