package de.f4ls3.game.entity;

import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.states.impl.PlayState;
import de.f4ls3.game.utils.DrawingUtils;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;

public abstract class Item extends Entity {

    protected boolean pickedUp, hitbox;
    protected int count;
    protected String name;
    protected Sprite sprite;

    public Item(Sprite sprite, Vector2f vec, int size, boolean hitbox, int count, String name) {
        super(sprite, vec, size);
        this.sprite = sprite;
        this.hitbox = hitbox;
        this.count = count;
        this.name = name;
        bounds.setWidth(size);
        bounds.setHeight(size);
    }

    @Override
    public void update() {
        super.update();

        if(bounds.collides(PlayState.getPlayer().bounds) && !pickedUp) {
            pickedUp = true;
        }
        if(bounds.collides(PlayState.getPlayer().bounds) && !pickedUp) {
            pickedUp = true;
        }
    }

    @Override
    public void render(Graphics2D graphics) {
        if(hitbox) {
            graphics.setColor(Color.CYAN);
            graphics.drawRect((int) (this.vec.getWorldVar().getX() + bounds.getXOffset()), (int) (this.vec.getWorldVar().getY() + bounds.getYOffset()), (int) bounds.getWidth(), (int) bounds.getHeight());
        }
        graphics.drawImage(sprite.getSprite(0, 0), (int) (this.vec.getWorldVar().getX()), (int) (this.vec.getWorldVar().getY()), size, size, null);
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public Sprite getSprite() {
        return sprite;
    }
}
