package de.f4ls3.game.entity.items;

import de.f4ls3.game.entity.Item;
import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.states.impl.PlayState;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;

public class PotionItem extends Item {

    public PotionItem(Vector2f vec, int size, boolean hitbox, int count) {
        super(new Sprite("sprites/potion.png", 32, 32), vec, size, hitbox, count, "Potion");
        bounds.setWidth(34);
        bounds.setHeight(45);
        bounds.setXOffset(12);
        bounds.setYOffset(10);
    }

    @Override
    public void render(Graphics2D graphics) {
        super.render(graphics);
    }

    @Override
    public void update() {
        super.update();

        if(pickedUp) {
            PlayState.getInventory().addItem(this, count);
            PlayState.getEntityManager().removeEntity(this);
        }
    }
}
