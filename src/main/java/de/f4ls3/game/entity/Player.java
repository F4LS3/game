package de.f4ls3.game.entity;

import de.f4ls3.game.GamePanel;
import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.states.impl.PlayState;
import de.f4ls3.game.utils.DrawingUtils;
import de.f4ls3.game.utils.KeyHandler;
import de.f4ls3.game.utils.MouseHandler;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.text.DecimalFormat;

public class Player extends Entity {

    private DecimalFormat f = new DecimalFormat("#0.00");

    public Player(Sprite sprite, Vector2f vec, int size) {
        super(sprite, vec, size);
        bounds.setWidth(55);
        bounds.setHeight(62);
        maxSpeed = 2f;
        acceleration = 1.3f;
        deceleration = 0.15f;
    }

    public void update() {
        super.update();
        move();
        if(this.currentAnimation == 0 ) { // RIGHT
            bounds.setXOffset(16);
            bounds.setYOffset(5);
        } else if(this.currentAnimation == 1) { // LEFT
            bounds.setXOffset(4);
            bounds.setYOffset(5);
        } else if(this.currentAnimation == 2 || this.currentAnimation == 3) { // UP
            bounds.setYOffset(8);
            bounds.setXOffset(8);
        } else {
            bounds.setXOffset(18);
            bounds.setYOffset(5);
        }

        if(!bounds.collisionTile(directionX, 0)) {
            PlayState.map.addX(this.directionX);
            this.vec.addX(this.directionX);
        }
        if(!bounds.collisionTile(0, directionY)) {
            PlayState.map.addY(this.directionY);
            this.vec.addY(this.directionY);
        }
    }

    @Override
    public void render(Graphics2D graphics) {
        DrawingUtils.drawFont(graphics, "X: " + f.format(this.vec.getX()).replace(",", ".") + " Y: " + f.format(this.vec.getY()).replace(",", "."), new Vector2f(20, 40), 32, 32, 24);
        graphics.setColor(Color.CYAN);
        graphics.drawRect((int) (this.vec.getWorldVar().getX() + bounds.getXOffset()), (int) (this.vec.getWorldVar().getY() + bounds.getYOffset()), (int) bounds.getWidth(), (int) bounds.getHeight());
        graphics.drawImage(this.animation.getImage(), (int) (this.vec.getWorldVar().getX()), (int) (this.vec.getWorldVar().getY()), size, size, null);
    }

    public void input(KeyHandler key, MouseHandler mouse) {
        if (key.up.down) {
            this.up = true;
        } else {
            this.up = false;
        }

        if (key.down.down) {
            this.down = true;
        } else {
            this.down = false;
        }

        if (key.right.down) {
            this.right = true;
        } else {
            this.right = false;
        }

        if (key.left.down) {
            this.left = true;
        } else {
            this.left = false;
        }
    }

    public void move() {
        if (up) {
            this.directionY -= this.acceleration;
            if (this.directionY < -this.maxSpeed) {
                this.directionY = -this.maxSpeed;
            }
        } else {
            if (this.directionY < 0) {
                this.directionY += this.deceleration;
                if (this.directionY > 0) {
                    this.directionY = 0;
                }
            }
        }

        if (down) {
            this.directionY += this.acceleration;
            if (this.directionY > this.maxSpeed) {
                this.directionY = this.maxSpeed;
            }
        } else {
            if (this.directionY > 0) {
                this.directionY -= this.deceleration;
                if (this.directionY < 0) {
                    this.directionY = 0;
                }
            }
        }

        if (right) {
            this.directionX += this.acceleration;
            if (this.directionX > this.maxSpeed) {
                this.directionX = this.maxSpeed;
            }
        } else {
            if (this.directionX > 0) {
                this.directionX -= this.deceleration;
                if (this.directionX < 0) {
                    this.directionX = 0;
                }
            }
        }

        if (left) {
            this.directionX -= this.acceleration;
            if (this.directionX < -this.maxSpeed) {
                this.directionX = -this.maxSpeed;
            }
        } else {
            if (this.directionX < 0) {
                this.directionX += this.deceleration;
                if (this.directionX > 0) {
                    this.directionX = 0;
                }
            }
        }
    }
}
