package de.f4ls3.game;

import de.f4ls3.game.graphics.Sprite;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame {

    public Window() {
        //setUndecorated(true);
        setTitle("Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(new GamePanel(1280, 720));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        //setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);

        Sprite sprite = new Sprite("sprites/normal_cursor.png", 16, 16);
        setCursor(Toolkit.getDefaultToolkit().createCustomCursor(sprite.getSprite(0, 0), new Point(0, 0), "normal cursor"));
    }
}
