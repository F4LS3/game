package de.f4ls3.game.tiles.blocks;

import de.f4ls3.game.collision.AxisAlignedBoundingBox;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.awt.image.BufferedImage;

public class HoleBlock extends Block {

    public HoleBlock(BufferedImage image, Vector2f vec, int w, int h) {
        super(image, vec, w, h);
    }

    @Override
    public boolean update(AxisAlignedBoundingBox aabb) {
        return false;
    }

    @Override
    public void render(Graphics2D graphics) {
        super.render(graphics);
        graphics.setColor(Color.green);
        graphics.drawRect((int) vec.getWorldVar().getX(), (int) vec.getWorldVar().getY(), w, h);
    }
}
