package de.f4ls3.game.tiles.blocks;

import de.f4ls3.game.collision.AxisAlignedBoundingBox;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Block {

    protected int w, h;
    protected BufferedImage image;
    protected Vector2f vec;

    public Block(BufferedImage image, Vector2f vec, int w, int h) {
        this.image = image;
        this.vec = vec;
        this.w = w;
        this.h = h;
    }

    public abstract boolean update(AxisAlignedBoundingBox aabb);

    public void render(Graphics2D graphics) {
        graphics.drawImage(image, (int) vec.getWorldVar().getX(), (int) vec.getWorldVar().getY(), w, h, null);
    }
}
