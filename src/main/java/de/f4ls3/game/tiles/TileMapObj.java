package de.f4ls3.game.tiles;

import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.tiles.blocks.Block;
import de.f4ls3.game.tiles.blocks.HoleBlock;
import de.f4ls3.game.tiles.blocks.ObjBlock;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.util.HashMap;

public class TileMapObj extends TileMap {

    public static HashMap<String, Block> tmo_blocks;

    public TileMapObj(String data, Sprite sprite, int width, int height, int tileWidth, int tileHeight, int tileColumns) {
        Block tempBlock;
        tmo_blocks = new HashMap<>();

        String[] block = data.split(",");
        try {
            for (int i = 0; i < (width * height); i++) {
                int temp = Integer.parseInt(block[i].replaceAll("\\s+", ""));
                if (temp != 0) {
                    if (temp == 172) {
                        int x = ((temp - 1) % tileColumns), y = ((temp - 1) / tileColumns);
                        tempBlock = new HoleBlock(sprite.getSprite(x, y), new Vector2f((i % width) * tileWidth, (i / height) * tileHeight), tileWidth, tileHeight);
                    } else {
                        int x = (temp - 1) % tileColumns, y = (temp - 1) / tileColumns;
                        tempBlock = new ObjBlock(sprite.getSprite(x, y), new Vector2f((i % width) * tileWidth, (i / height) * tileHeight), tileWidth, tileHeight);
                    }
                    tmo_blocks.put(i % width + "," + i / height, tempBlock);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void render(Graphics2D graphics) {
        for (Block block : tmo_blocks.values()) {
            block.render(graphics);
        }
    }
}
