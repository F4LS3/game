package de.f4ls3.game.tiles;

import de.f4ls3.game.tiles.blocks.Block;
import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.tiles.blocks.NormBlock;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TileMapNorm extends TileMap {

    private List<Block> blocks;

    public TileMapNorm(String data, Sprite sprite, int width, int height, int tileWidth, int tileHeight, int tileColumns) {
        blocks = new ArrayList<>();

        String[] block = data.split(",");
        for (int i = 0; i < (width * height); i++) {
            int temp = Integer.parseInt(block[i].replaceAll("\\s+", ""));
            if(temp != 0) {
                int x = (temp - 1) % tileColumns, y = ((temp - 1)) / tileColumns;
                blocks.add(new NormBlock(sprite.getSprite(x, y), new Vector2f((i % width) * tileWidth, (i / height) * tileHeight), tileWidth, tileHeight));
            }
        }
    }

    @Override
    public void render(Graphics2D graphics) {
        for (int i = 0; i < blocks.size(); i++) {
            blocks.get(i).render(graphics);
        }
    }
}
