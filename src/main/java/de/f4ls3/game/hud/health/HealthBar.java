package de.f4ls3.game.hud.health;

import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.utils.DrawingUtils;
import de.f4ls3.game.utils.Logger;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;

public class HealthBar {

    private int health = 10, oldHealth;
    private Sprite sprite;
    private Vector2f vector;

    public HealthBar() {
        this.sprite = new Sprite("sprites/health/health_bar_" + health + ".png", 98, 20);
        this.vector = new Vector2f(20, 525);
    }

    public void update() {
        if(health != oldHealth)
            this.sprite = new Sprite("sprites/health/health_bar_" + health + ".png", 98, 20);
            oldHealth = health;
    }

    public void render(Graphics2D graphics) {
        DrawingUtils.drawImage(graphics, sprite.getSprite(), vector, 98 * 3, 20 * 3);
    }

    public void heal(int amount) {
        if((health + amount) > 10) return;
        oldHealth = health;
        health += amount;
    }

    public void damage(int amount) {
        if((health - amount) < 0) {
            health = 0;
            return;
        }
        oldHealth = health;
        health -= amount;
        Logger.log("Removed " + amount + " health");
    }
}
