package de.f4ls3.game.hud.inventory;

import de.f4ls3.game.entity.Item;
import de.f4ls3.game.utils.DrawingUtils;
import de.f4ls3.game.utils.Vector2f;

import java.awt.Graphics2D;

public class Slot {

    protected Item item;

    protected Vector2f vector;
    protected int index;
    private boolean renderItem;

    public Slot(Vector2f vector, int index) {
        this.vector = vector;
        this.index = index;
    }

    public void update() {
        renderItem = this.item != null;
    }

    public void render(Graphics2D graphics) {
        if (renderItem) {
            DrawingUtils.drawImage(graphics, this.item.getSprite().getSprite(), this.vector, 64, 64);
            DrawingUtils.drawFont(graphics, this.item.getCount() + "", new Vector2f(this.vector.getX() + 35, this.vector.getY() + 40), 16, 16, 20);
        }
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Item getItem() {
        return item;
    }
}
