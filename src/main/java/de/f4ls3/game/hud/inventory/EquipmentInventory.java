package de.f4ls3.game.hud.inventory;

import de.f4ls3.game.entity.Item;
import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.utils.DrawingUtils;
import de.f4ls3.game.utils.Vector2f;

import java.awt.*;
import java.util.HashMap;

public class EquipmentInventory {

    private static HashMap<Item, Integer> inventory = new HashMap<>();
    private static Sprite sprite;
    private static Vector2f vector;

    private static Item[] items;
    private static Slot[] slots;

    public EquipmentInventory() {
        this.sprite = new Sprite("sprites/equipment.png", 40, 200);
        this.vector = new Vector2f(50, 110);

        items = new Item[3];
        slots = new Slot[]{new Slot(new Vector2f(55, 610), 0), new Slot(new Vector2f(142, 610), 1),
                new Slot(new Vector2f(95, 610), 2), new Slot(new Vector2f(115, 610), 3)};
    }

    public void addItem(Item item, int amount) {
        if (inventory.containsKey(item)) {
            if (amount != -1) {
                inventory.replace(item, amount);
            } else {
                inventory.replace(item, 1);
            }
        } else {
            if (amount != -1) {
                inventory.put(item, 1);
            } else {
                inventory.put(item, 1);
            }
        }
    }

    public void removeItem(Item item, int amount) {
        if (inventory.containsKey(item)) {
            if (amount == -1) {
                inventory.remove(item);
            } else {
                int ref = inventory.get(item) - amount;
                if (ref <= 0) {
                    inventory.remove(item);
                } else {
                    inventory.replace(item, ref);
                }
            }
        }
    }

    public void render(Graphics2D graphics) {
        DrawingUtils.drawImage(graphics, this.sprite.getSprite(), this.vector, 80, 400);

        for (Slot slot : slots) {
            slot.render(graphics);
        }
    }

    public void update() {
        int count = 0;
        for (Item item : inventory.keySet()) {
            items[count] = item;
            slots[count].setItem(items[count]);
            count++;
        }

        for (Slot slot : slots) {
            slot.update();
        }
    }
}
