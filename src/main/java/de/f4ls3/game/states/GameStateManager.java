package de.f4ls3.game.states;

import de.f4ls3.game.GamePanel;
import de.f4ls3.game.states.impl.DeadState;
import de.f4ls3.game.states.impl.LoadingState;
import de.f4ls3.game.states.impl.MenuState;
import de.f4ls3.game.states.impl.PlayState;
import de.f4ls3.game.utils.KeyHandler;
import de.f4ls3.game.utils.MouseHandler;
import de.f4ls3.game.utils.Vector2f;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public class GameStateManager {

    private List<GameState> states;

    public static Vector2f map;

    public static final int MENU = 0;
    public static final int LOADING = 1;
    public static final int PLAY = 2;
    public static final int DEAD = 3;

    public GameStateManager() {
        this.map = new Vector2f(GamePanel.getWindowWidth(), GamePanel.getWindowHeight());
        Vector2f.setWorldVar(this.map.getX(), this.map.getY());
        this.states = new ArrayList<>();

        this.states.add(0, new MenuState(this));
        this.states.add(1, new LoadingState(this));
        this.states.add(2, new PlayState(this));
        this.states.add(3, new DeadState(this));
    }

    public void pop(int state) {
        this.states.remove(state);
    }

    public void update() {
        Vector2f.setWorldVar(this.map.getX(), this.map.getY());
        for (int i = 0; i < this.states.size(); i++) this.states.get(i).update();
    }

    public void input(MouseHandler mouse, KeyHandler key) {
        for (int i = 0; i < this.states.size(); i++) this.states.get(i).input(mouse, key);
    }

    public void render(Graphics2D graphics) {
        for (int i = 0; i < this.states.size(); i++) this.states.get(i).render(graphics);
    }
}
