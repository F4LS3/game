package de.f4ls3.game.states;

import de.f4ls3.game.utils.KeyHandler;
import de.f4ls3.game.utils.MouseHandler;

import java.awt.Graphics2D;

public abstract class GameState {

    private GameStateManager gsm;

    protected GameState(GameStateManager gsm) {
        this.gsm = gsm;
    }

    public abstract void update();
    public abstract void input(MouseHandler mouse, KeyHandler key);
    public abstract void render(Graphics2D graphics);

    public GameStateManager getGsm() {
        return gsm;
    }
}
