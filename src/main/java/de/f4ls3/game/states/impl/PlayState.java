package de.f4ls3.game.states.impl;

import de.f4ls3.game.GamePanel;
import de.f4ls3.game.entity.Player;
import de.f4ls3.game.graphics.Sprite;
import de.f4ls3.game.hud.health.HealthBar;
import de.f4ls3.game.hud.inventory.EquipmentInventory;
import de.f4ls3.game.hud.inventory.Inventory;
import de.f4ls3.game.states.GameState;
import de.f4ls3.game.states.GameStateManager;
import de.f4ls3.game.utils.*;

import java.awt.*;

public class PlayState extends GameState {

    private TileManager tileManager;

    private static Player player;
    private static EntityManager entityManager;
    private static Inventory inventory;
    private static EquipmentInventory equipmentInventory;
    private static HealthBar healthBar;

    public static Vector2f map;

    public PlayState(GameStateManager gsm) {
        super(gsm);
        this.tileManager = new TileManager("map2.xml");
        this.entityManager = new EntityManager();
        this.player = new Player(new Sprite("spritesheets/playerSheet.png", 32, 32), new Vector2f(0 + (GamePanel.getWindowWidth() / 2) - 32, 0 + (GamePanel.getWindowHeight() / 2) - 32), 76);
        getEntityManager().registerEntities();
        this.inventory = new Inventory();
        this.equipmentInventory = new EquipmentInventory();
        this.healthBar = new HealthBar();
        map = new Vector2f();
        Vector2f.setWorldVar(map.getX(), map.getY());
    }

    @Override
    public void update() {
        Vector2f.setWorldVar(map.getX(), map.getY());

        getEntityManager().updateEntities();
        getInventory().update();
        getEquipmentInventory().update();
        getHealthBar().update();
        getPlayer().update();
    }

    @Override
    public void input(MouseHandler mouse, KeyHandler key) {
        getPlayer().input(key, mouse);
        getEntityManager().inputEntities(key, mouse);
        getInventory().input(key, mouse);
    }

    @Override
    public void render(Graphics2D graphics) {
        this.tileManager.render(graphics); // <- Has to be rendered the very first
        DrawingUtils.drawFont(graphics, GamePanel.oldFrameCount + " FPS", new Vector2f(20, 10), 32, 32, 24);

        getEntityManager().renderEntities(graphics);
        getInventory().render(graphics);
        getEquipmentInventory().render(graphics);
        getHealthBar().render(graphics);
        getPlayer().render(graphics); // <- Has to be rendered the very last
    }

    public static Inventory getInventory() {
        return inventory;
    }

    public static EquipmentInventory getEquipmentInventory() {
        return equipmentInventory;
    }

    public static HealthBar getHealthBar() {
        return healthBar;
    }

    public static Player getPlayer() {
        return player;
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }
}
