package de.f4ls3.game.states.impl;

import de.f4ls3.game.states.GameState;
import de.f4ls3.game.states.GameStateManager;
import de.f4ls3.game.utils.KeyHandler;
import de.f4ls3.game.utils.MouseHandler;

import java.awt.*;

public class DeadState extends GameState {

    public DeadState(GameStateManager gsm) {
        super(gsm);
    }

    @Override
    public void update() {

    }

    @Override
    public void input(MouseHandler mouse, KeyHandler key) {

    }

    @Override
    public void render(Graphics2D graphics) {

    }
}
