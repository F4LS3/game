package de.f4ls3.game.utils;

import de.f4ls3.game.GamePanel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class KeyHandler implements KeyListener {

    public List<Key> keys = new ArrayList<>();

    public class Key {
        public int presses, absorbs;
        public boolean down, clicked;

        public Key() {
            keys.add(this);
        }

        public void toggle(boolean pressed) {
            if (pressed != down) {
                down = pressed;
            }
            if (pressed) {
                presses++;
            }
        }

        public void tick() {
            if (absorbs < presses) {
                absorbs++;
                clicked = true;
            } else {
                clicked = false;
            }
        }
    }

    public Key up = new Key(); // W
    public Key down = new Key(); // S
    public Key left = new Key(); // A
    public Key right = new Key(); // D
    public Key interact = new Key(); // E
    public Key attack = new Key(); // SPACE
    public Key escape = new Key(); // ESC
    public Key itemMenu = new Key(); // F8
    public Key arrowUp = new Key(); // UP ARROW
    public Key arrowDown = new Key(); // DOWN ARROW
    public Key arrowLeft = new Key(); // LEFT ARROW
    public Key arrowRight = new Key(); // RIGHT ARROW
    public Key enter = new Key(); // ENTER
    public Key one = new Key();
    public Key two = new Key();
    public Key three = new Key();
    public Key four = new Key();

    public KeyHandler(GamePanel gamePanel) {
        gamePanel.addKeyListener(this);
    }

    public void releaseAll() {
        for (int i = 0; i < keys.size(); i++) {
            keys.get(i).down = false;
        }
    }

    public void tick() {
        for (int i = 0; i < keys.size(); i++) {
            keys.get(i).tick();
        }
    }

    public void toggle(KeyEvent event, boolean pressed) {
        if (event.getKeyCode() == KeyEvent.VK_W) up.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_A) left.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_S) down.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_D) right.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_E) interact.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_SPACE) attack.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_ESCAPE) escape.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_F8) itemMenu.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_UP) arrowUp.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_DOWN) arrowDown.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_LEFT) arrowLeft.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_RIGHT) arrowRight.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_ENTER) enter.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_1 || event.getKeyCode() == KeyEvent.VK_NUMPAD1) one.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_2 || event.getKeyCode() == KeyEvent.VK_NUMPAD2) two.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_3 || event.getKeyCode() == KeyEvent.VK_NUMPAD3) three.toggle(pressed);
        if (event.getKeyCode() == KeyEvent.VK_4 || event.getKeyCode() == KeyEvent.VK_NUMPAD4) four.toggle(pressed);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // Do Nothing
    }

    @Override
    public void keyPressed(KeyEvent e) {
        toggle(e, true);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        toggle(e, false);
    }
}
