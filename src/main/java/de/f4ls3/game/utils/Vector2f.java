package de.f4ls3.game.utils;

public class Vector2f {

    private float x, y;

    public static float worldX, worldY;

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2f(Vector2f vec) {
        this.x = vec.x;
        this.y = vec.y;
    }

    public Vector2f() {
        this.x = 0;
        this.y = 0;
    }

    public Vector2f addX(float x) {
        this.x += x;
        return this;
    }

    public Vector2f addY(float y) {
        this.y += y;
        return this;
    }

    public Vector2f setX(float x) {
        this.x = x;
        return this;
    }

    public Vector2f setY(float y) {
        this.y = y;
        return this;
    }

    public Vector2f setVector(Vector2f vec) {
        this.x = vec.x;
        this.y = vec.y;
        return this;
    }

    public static void setWorldVar(float worldX, float worldY) {
        Vector2f.worldX = worldX;
        Vector2f.worldY = worldY;
    }

    public Vector2f getWorldVar() {
        return new Vector2f(x - worldX, y - worldY);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    @Override
    public String toString() {
        return "X: " + this.x + ", Y: " + this.y;
    }
}
