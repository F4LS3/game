package de.f4ls3.game.utils;

import de.f4ls3.game.graphics.Font;
import de.f4ls3.game.graphics.Sprite;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class DrawingUtils {

    private static Font font = new Font("font.png", 10, 10);

    public static void drawFont(Graphics2D graphics, String text, Vector2f vec, int width, int height, int xOffset) {
        Sprite.drawArray(graphics, font, text, vec, width, height, xOffset, 0);
    }

    public static void drawImage(Graphics2D graphics, BufferedImage image, Vector2f vec, int width, int height) {
        graphics.drawImage(image, (int) vec.getX(), (int) vec.getY(), width, height, null);
    }
}
