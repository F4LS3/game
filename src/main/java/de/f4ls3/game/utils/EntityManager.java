package de.f4ls3.game.utils;

import de.f4ls3.game.GamePanel;
import de.f4ls3.game.entity.Entity;
import de.f4ls3.game.entity.Item;
import de.f4ls3.game.entity.items.BerryItem;
import de.f4ls3.game.entity.items.PotionItem;
import de.f4ls3.game.graphics.Sprite;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class EntityManager {

    private List<Entity> entities;
    private List<Entity> toRemove;

    public EntityManager() {
        this.entities = new ArrayList<>();
        this.toRemove = new ArrayList<>();
    }

    public void registerEntities() {
        createEntity(new BerryItem(new Vector2f(0 + (GamePanel.getWindowWidth() / 2) - 40, 0 + (GamePanel.getWindowHeight() / 2) + 100), 64, true, 4));
        createEntity(new PotionItem(new Vector2f(0 + (GamePanel.getWindowHeight() / 2) + 50, 0 + 0 + (GamePanel.getWindowHeight() / 2) + 120), 64, true, 2));
    }

    public void renderEntities(Graphics2D graphics) {
        for (Entity entity : entities) {
            entity.render(graphics);
        }
    }

    public void inputEntities(KeyHandler key, MouseHandler mouse) {
        for (Entity entity : entities) {
            if (!(entity instanceof Item)) {
                entity.input(key, mouse);
            }
        }
    }

    public void updateEntities() {
        for (Entity entity : entities) {
            entity.update();
        }
        if (!toRemove.isEmpty())
            entities.removeAll(toRemove);
        toRemove.clear();
    }

    public void createEntity(Entity entity) {
        entities.add(entity);
    }

    public void removeEntity(Entity entity) {
        if (entities.contains(entity)) {
            toRemove.add(entity);
        }
        Logger.log("Removed " + (toRemove.size()) + " items");
    }

    public List<Entity> getEntities() {
        return entities;
    }
}
