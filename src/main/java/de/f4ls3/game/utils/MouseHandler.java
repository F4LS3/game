package de.f4ls3.game.utils;

import de.f4ls3.game.GamePanel;

import java.awt.event.*;

public class MouseHandler implements MouseListener, MouseMotionListener, MouseWheelListener {

    private static int mouseX = -1, mouseY = -1, mouseB = -1, mouseWheelBefore = 0;
    private static boolean up, down;

    public MouseHandler(GamePanel gamePanel) {
        gamePanel.addMouseListener(this);
        gamePanel.addMouseWheelListener(this);
        gamePanel.addMouseMotionListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.mouseB = e.getButton();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.mouseB = -1;
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        this.mouseX = e.getX();
        this.mouseY = e.getY();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.mouseX = e.getX();
        this.mouseY = e.getY();
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if(e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
            if(e.getWheelRotation() < mouseWheelBefore) {
                up = true;

            } else if(e.getWheelRotation() > mouseWheelBefore) {
                down = true;
            }
            mouseWheelBefore = e.getWheelRotation();
        }
        mouseWheelBefore = 0;
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    public int getMouseButton() {
        return mouseB;
    }

    public boolean isUp() {
        boolean temp = up;
        up = false;
        return temp;
    }

    public boolean isDown() {
        boolean temp = down;
        down = false;
        return temp;
    }
}
