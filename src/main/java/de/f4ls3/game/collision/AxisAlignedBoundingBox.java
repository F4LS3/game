package de.f4ls3.game.collision;

import de.f4ls3.game.entity.Entity;
import de.f4ls3.game.tiles.TileMapObj;
import de.f4ls3.game.utils.Vector2f;

import javax.swing.*;

public class AxisAlignedBoundingBox {

    private Vector2f vec;
    private Entity entity;
    private float width, height, visionRadius, xOffset, yOffset;
    private int size;

    public AxisAlignedBoundingBox(Vector2f vec, int width, int height) {
        this.vec = vec;
        this.width = width;
        this.height = height;
        this.size = Math.max(width, height);
    }

    public AxisAlignedBoundingBox(Vector2f vec, Entity entity, int visionRadius) {
        this.vec = vec;
        this.entity = entity;
        this.visionRadius = visionRadius;
        this.size = visionRadius;
    }

    public Vector2f getVector() {
        return this.vec;
    }

    public float getRadius() {
        return this.visionRadius;
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public float getXOffset() {
        return xOffset;
    }

    public float getYOffset() {
        return yOffset;
    }

    public void setBox(Vector2f vec, int width, int height) {
        this.vec = vec;
        this.width = width;
        this.height = height;

        this.size = Math.max(width, height);
    }

    public void setCircle(Vector2f vec, int radius) {
        this.vec = vec;
        this.visionRadius = radius;
        this.size = radius;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setXOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public void setYOffset(float yOffset) {
        this.yOffset = yOffset;
    }

    public boolean collides(AxisAlignedBoundingBox aabb) {
        float ax = ((this.vec.getWorldVar().getX() + (this.xOffset)) + (this.width / 2));
        float ay = ((this.vec.getWorldVar().getY() + (yOffset)) + (this.height / 2));
        float bx = ((aabb.vec.getWorldVar().getX() + (aabb.xOffset / 2)) + (this.width / 2));
        float by = ((aabb.vec.getWorldVar().getY() + (aabb.yOffset / 2)) + (this.height / 2));

        return Math.abs(ax - bx) < (this.width / 2) + (aabb.width / 2) && Math.abs(ay - by) < (this.height / 2) + (aabb.height / 2);
    }

    public boolean collideCircleBox(AxisAlignedBoundingBox aabb) {
        float cx = (float) (this.vec.getWorldVar().getX() + this.visionRadius / Math.sqrt(2) - this.entity.getSize() / Math.sqrt(2));
        float cy = (float) (this.vec.getWorldVar().getY() + this.visionRadius / Math.sqrt(2) - this.entity.getSize() / Math.sqrt(2));

        float xDelta = cx - Math.max(aabb.vec.getWorldVar().getX() + (aabb.getWidth() / 2), Math.min(cx, aabb.vec.getWorldVar().getX()));
        float yDelta = cy - Math.max(aabb.vec.getWorldVar().getY() + (aabb.getHeight() / 2), Math.min(cy, aabb.vec.getWorldVar().getY()));

        return (xDelta * xDelta + yDelta * yDelta) < ((this.visionRadius / Math.sqrt(2)) * (this.visionRadius / Math.sqrt(2)));
    }

    public boolean collisionTile(float ax, float ay) {
        for (int i = 0; i < 4; i++) {
            int xt = (int) ((this.vec.getX() + ax) + (i % 2) * this.width + this.xOffset) / 64;
            int yt = (int) ((this.vec.getY() + ay) + (i / 2) * this.height + this.yOffset) / 64;

            if (TileMapObj.tmo_blocks.containsKey(xt + "," + yt)) {
                return TileMapObj.tmo_blocks.get(xt + "," + yt).update(this);
            }
        }

        return false;
    }
}
